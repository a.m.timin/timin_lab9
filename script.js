// 1
const myApp = {
    data() {
        return {
            Первый: typeof(9),
            Второй: typeof(1.2),
            Третий: typeof(NaN),
            Четвертый: typeof("Hello World"),
            Пятый: typeof(true),
            Шестой: typeof(2 != 1),
            Седьмой: "сыр" + "ы",
            Восьмой: "сыр" - "ы",
            Девятый: "2" + "4",
            Десятый: "2" - "4",
            Одиннадцатый: "Сэм" + 5, 
            Двенадцатый: "Сэм" - 5,
            Тринадцатый: 99 * "шары"
        }
    }
};
Vue.createApp(myApp).mount('#app');

// 2
function calc() {
    var side1 = +document.getElementById("s1").value;
    var side2 = +document.getElementById("s2").value;
    var perimeter = (side1 * 2 + side2 * 2 );
    var square = (side1 * side2)
//     document.getElementById("answer1").innerHTML = "Perimeter: " + perimeter;
//     document.getElementById("answer2").innerHTML = "Square: " + square;
}

// 3
function celsresult() {

    var x, y, z;

    x = document.getElementById('cels').value;
    x = parseInt(x);

    y = x*1.8+32;

    z = (x + " градусов это " + y + " по фаренгейту");

//     document.getElementById('celsout').innerHTML = z;
}

    function fahresult() {

    var x, y, z;

    x = document.getElementById('fah').value;
    x = parseInt(x);

    y = (x - 32)/1.8;

    z = (x + " градусов это " + y + " цельсия");

//     document.getElementById('fahout').innerHTML = z;
}

// 4
function isLeapYear (year) {
    return (year % 100 === 0) ? (year % 400 === 0) : (year % 4 === 0);
}
var year = +prompt("Введи год:");
if(isNaN(year)){
    alert("Нужно ввести целое число.")
}
if(isLeapYear(year)){
    alert("Год " + year + " - високосный.");
} else {
    alert("Год " + year + " - не високосный.");
}

// 5
let num = 2;
let num2 = 8;
let flag = ((num==10) || (num2==10) || ((num1+num2) == 10));
console.log(Boolean(flag));


// 6
let numberSheep = prompt("enter the number of sheep");
let s="";
let i = 1;
function sum() {
    s += i;
    s += " sheep...";
};
for (i; i <= numberSheep; i++) {
    sum();
}
console.log(numberSheep);
console.log(s);

// 7
for (let k = 0; k <16; k++) {
    (k$2 !=0 ) ? console.log(' ${k} odd number ') : console.log(' ${k} even number ');
}

// 8
let j = 0;
function chet(j) {
    console.log("*".repeat(j));
};
function nechet(j) {
    console.log("#".repeat(j));
};
for (j; j<12; j++) {
    (j%2 != 0) ? chet(j) : nechet(j);
};

// 9
let arr = [34, -1, 2];
arr.sort((a,b) => a-b);
console.log(arr);

// 10
console.log(Math.max(7, -1, 0, -8, -4));